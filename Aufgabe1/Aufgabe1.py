#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure



# ### 1)

# In[2]:


def label(data):
    label = np.ones(len(data))
    gz = np.where(data > [0,0])
    
    # 2. Quadrant
    q2 = np.intersect1d(np.where(data[:,0] > 0)[0],   # x - axis
                        np.where(data[:,1] > 0)[0] )  # y - axis
    
    # 4. Quadrant
    q4 = np.intersect1d(np.where(data[:,0] < 0)[0],   # x - axis
                        np.where(data[:,1] < 0)[0] )  # y - axis
    label[q2] = 0
    label[q4] = 0
    return label

def draw_sample_uniform(low = -6, high = 6,samples = 200):
    x = np.random.uniform(low,high,samples)
    y = np.random.uniform(low,high,samples)    
    return np.array([x,y]).T


# In[3]:


train_x = draw_sample_uniform()
test_x  = draw_sample_uniform()
train_y = label(train_x)
test_y  = label(test_x)


# In[4]:


def plot(train_x,train_y,test_x,test_y):
    fig, axes = plt.subplots(1, 2, figsize=(15, 5),dpi=60, sharex=False, sharey=True)
    color = ['red','blue']
    for i in range(len(train_x)):
        axes[0].scatter(train_x[i,0],
                      train_x[i,1],
                      alpha=0.8, 
                      edgecolors='none', 
                      s=20,
                      c = color[int(train_y[i])])
        if i < len(test_x):
            axes[1].scatter(test_x[i,0],
                          test_x[i,1],
                          alpha=0.8, 
                          edgecolors='none', 
                          s=20,
                          c = color[int(test_y[i])])
    axes[1].grid(linestyle='--')
    axes[1].set_title("Testset")
    axes[0].grid(linestyle='--')
    axes[0].set_title("Trainset")
    
    plt.show()
    

plot(train_x,train_y,test_x,test_y)


# ### 4)
# 
# Das in der Vorlesung dargestellte Experiment operiert nicht direkt auf den Inputdaten,sondern auf 2 Merkmalen, die mithilfe zweier Neuronen mit fixem Gewichtsvektorberechnet werden: ein Neuron teilt die Inputebene waagrecht entlang der x-Achse, dasandere senkrecht entlang der y-Achse. Wie muss der Gewichtsvektor für das jeweiligeNeuron aussehen?
# 
#  - Die Daten sind doch schon in X und Y-Achse aufgeteilt? höchstens skalieren?

# In[5]:


# Sigmoid (vektorisiert)
def sigmoid(z):
    """The sigmoid function."""
    return 1.0/(1.0+np.exp(-z))


# In[6]:


train_x_100_100 = draw_sample_uniform(samples=100*100)
train_y_100_100 = label(train_x)

sigmoid_train = sigmoid(train_x_100_100)


# In[7]:


cm = plt.cm.get_cmap('RdYlBu')
fig, axes = plt.subplots(1, 2, figsize=(15, 5),dpi=60, sharex=False, sharey=True)
sc0 = axes[0].scatter(train_x_100_100[:,0],train_x_100_100[:,1],c=sigmoid_train[:,0],s=20)
sc1 = axes[1].scatter(train_x_100_100[:,0],train_x_100_100[:,1],c=sigmoid_train[:,1],s=20)
plt.show()


# In[8]:


# Sigmoid (vektorisiert)
def sigmoid(z):
    """The sigmoid function."""
    return 1.0/(1.0+np.exp(-z))

# Ableitung des Sigmoids
def sigmoid_prime(z):
    """Derivative of the sigmoid function."""
    return sigmoid(z)*(1-sigmoid(z))

# Ableitung der MSE-Kostenfunktion
def cost_derivative(output_activations, y):
    """Return the vector of partial derivatives \partial C_x /
    \partial a for the output activations."""
    return (output_activations-y)


# Erste Fundamentalgleichung:
# 
# $\delta^L = \nabla_a C \odot \sigma^{'} (z^L)$ 
# 
# zweite:
# 
# $\delta^l = ((w^{l+1})^T \delta^{l+1}) \odot \sigma^{'}(z^l)$
# 
# dritte:
# 
# $\frac{\partial C}{\partial b_{j}^{l}} = \delta^l_j$
# 
# vierte:
# 
# $\frac{\partial C}{\partial w_{jk}^{l}} = a_k^{l-1}\delta^l_j$

# In[ ]:


class NN():
    def __init__(self,**kwargs):
        self.shape = kwargs['shape']
        self.activation = kwargs['activation']
        self.derivative = kwargs['derivative']
        self.cost_derivative = kwargs['cost_derivative']
        self.lr = kwargs['lr']
        self.epochs = kwargs['epochs']
        self.batchsize = kwargs['batchsize']
        self.biases = [np.random.rand(y,1) for y in self.shape[1:]] 
        self.weights = [np.random.rand(y,x) for x,y in zip(self.shape[:-1],self.shape[1:])]
        print("Biases:\n")
        for b in self.biases:
            print(b.T.shape,end = " - ")
        
        print("\n\nWeights:\n")
        for w in self.weights:
            print(w.T.shape,end = " - ")
        
    def feedforward(self,input_nn):
        
        """ 
        
            This method is not used for backpropagation, only for predicting
        
        """
        a = input_nn
        for w,b in self.weights,self.biases:
            a = self.activation( (w@a) + b )
        return a
    
    def backprop(self,x,y):
        print("PROPOINPOPO")
        activation = x
        activations = [x]
        zs = []
        
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        print("HIERO")
        # forwardpass
        """
         calculate z layer by layer and store it in a list, we need the vectors
         for 
        """
        for b,w in zip(self.biases,self.weights):
            print(1)
            z = (w@activation)+b
            zs.append(z)
            activation = self.activation(z) # sigma(z)
            activations.append(activation)

        print("FERTIG")
            
        # backwardpass
        
        """
            
            delta^L respectively delta = first fundamental equation 
        
        """
        
        delta = self.cost_derivative(activations[-1],y) * self.derivative(zs[-1])
        
        nabla_b[-1] = delta
        nabla_w[-1] = delta @ activations[-2].transpose()
        
        print(nabla_w)
        print(nabla_b)
        
        
    
    def update_minibatch(self,batch):
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        
        for x,y in batch:
            delta_nabla_b,delta_nabla_w = self.backprop(x,y)
            return
        
    def fit(self,training_data,test_data=None):
        
        n = len(training_data)
        
        for epoch in range(self.epochs):
            print(epoch)
            np.random.shuffle(training_data)
            print("shuffle")
            mini_batches = [training_data[k:k+self.batchsize]
                            for k in range(0,n,self.batchsize)]
            print("mini")
            for mini_batch in mini_batches:
                self.update_minibatch(mini_batch)
                return
                
        

model = NN(shape=(2,3,4,1),
           activation = sigmoid,
           derivative = sigmoid_prime,
           cost_derivative = cost_derivative,
           lr = 0.03,
           epochs = 150,
           batchsize = 10)

def reshape_data(train_x,train_y):
    data = [(x,y) for x,y in zip(train_x,train_y)]
    return data
    
        
data = reshape_data(train_x_100_100,train_y_100_100)




model.fit( data )

