# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure



# Sigmoid (vektorisiert)
def sigmoid(z):
    """The sigmoid function."""
    return 1.0/(1.0+np.exp(-z))

# Ableitung des Sigmoids
def sigmoid_prime(z):
    """Derivative of the sigmoid function."""
    return sigmoid(z)*(1-sigmoid(z))

# Ableitung der MSE-Kostenfunktion
def cost_derivative(output_activations, y):
    """Return the vector of partial derivatives \partial C_x /
    \partial a for the output activations."""
    return (output_activations-y)




class NN():
    def __init__(self,**kwargs):
        self.shape = kwargs['shape']
        self.activation = kwargs['activation']
        self.derivative = kwargs['derivative']
        self.cost_derivative = kwargs['cost_derivative']
        self.lr = kwargs['lr']
        self.epochs = kwargs['epochs']
        self.batchsize = kwargs['batchsize']
        self.biases = [np.random.rand(y,1) for y in self.shape[1:]] 
        self.weights = [np.random.rand(y,x) for x,y in zip(self.shape[:-1],self.shape[1:])]
        print("Biases:\n")
        for b in self.biases:
            print(b.T.shape,end = " - ")
        
        print("\n\nWeights:\n")
        for w in self.weights:
            print(w.T.shape,end = " - ")
        
    def feedforward(self,input_nn):
        
        """ 
        
            This method is not used for backpropagation, only for predicting
        
        """
        a = input_nn
        for w,b in self.weights,self.biases:
            a = self.activation( (w@a) + b )
        return a
    
    def backprop(self,x,y):
        print("PROPOINPOPO")
        activation = x
        activations = [x]
        zs = []
        
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        
        # forwardpass
        """
         calculate z layer by layer and store it in a list, we need the vectors
         for 
        """
        for b,w in zip(self.biases,self.weights):
            z = (w@activation)+b
            zs.append(z)
            activation = self.activation(z) # sigma(z)
            activations.append(activations)
            
        # backwardpass
        
        """
            
            delta^L respectively delta = first fundamental equation 
        
        """
        
        delta = self.cost_derivative(activations[-1],y) * self.derivative(zs[-1])
        
        nabla_b[-1] = delta
        nabla_w[-1] = delta @ activations[-2].transpose()
        
        print(nabla_w)
        print(nabla_b)
        
        
    
    def update_minibatch(self,batch):
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        
        for x,y in batch:
            delta_nabla_b,delta_nabla_w = self.backprop(x,y)
            return
        
    def fit(self,training_data,test_data=None):
        
        n = len(training_data)
        
        for epoch in range(self.epochs):
            print(epoch)
            np.random.shuffle(training_data)
            print("shuffle")
            mini_batches = [training_data[k:k+self.batchsize]
                            for k in range(0,n,self.batchsize)]
            print("mini")
            for mini_batch in mini_batches:
                self.update_minibatch(mini_batch)
                return
                
        

def reshape_data(train_x,train_y):
    data = [(x,y) for x,y in zip(train_x,train_y)]
    return data
    
        





